<?php
class Comments{
    public $title = "";
    public $text = "";

    /**
     * Comments constructor.
     * @param string $title
     * @param string $text
     */
    public function __construct($title, $text)
    {
        $this->title = $title;
        $this->text = $text;
    }


};
